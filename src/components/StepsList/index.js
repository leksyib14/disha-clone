import React from 'react'
import './styles.css'

import articleIcon from '../../images/article-icon.png'

const StepsList = ({ title, subText }) => {
  return (
    <div className="item">
      <img className="image" alt="icon" src={articleIcon} width="32px" />
      <div className="text">
        <b>{title}</b>
        <p className="sub-text">{subText}</p>
      </div>
    </div>
  )
}

export default StepsList
