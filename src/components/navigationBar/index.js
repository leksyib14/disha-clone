import React from 'react'
import './styles.css'
import Button from '../Button'

const NavigationBar = () => {
  return (
    <div className="nav">
      <div className="wrapper flex-nav">
        <div className="logo">LOGO</div>
        <Button
          background="#FA7F46"
          buttonText="Log in to Favour Pages"
          height={50}
          width={175}
        />
      </div>
    </div>
  )
}

export default NavigationBar
