import React from 'react'

const Button = ({
  buttonText,
  width,
  height,
  marginTop,
  borderRadius,
  background,
  paddingBottom,
  buttonTextColor,
  marginRight,
}) => {
  return (
    <button
      style={{
        height,
        width,
        background,
        cursor: 'pointer',
        color: buttonTextColor ? buttonTextColor : '#fff',
        border: 0,
        fontSize: 12,
        fontWeight: 500,
        marginTop,
        borderRadius,
        paddingBottom,
        marginRight,
      }}
    >
      {buttonText}
    </button>
  )
}

export default Button
