import React from 'react'
import './styles.css'

const Article = () => {
  return (
    <div className="body-article">
      <h4 className="body-title">Favour Pages</h4>
      <h2 className="body-sub_text">
        Create a one-page site
        <br /> on your phone
      </h2>
      <h3>
        Create, publish and edit a beautiful
        <br />
        responsive website instantly. Zero code/
        <br />
        design thinking required.
      </h3>
      <ul className="article-list">
        <li>Add links, images, videos and text in sections</li>
        <li>Add links, images, videos and text in sections</li>
        <li>Add links, images, videos and text in sections</li>
        <li>Add links, images, videos and text in sections</li>
      </ul>
    </div>
  )
}

export default Article
