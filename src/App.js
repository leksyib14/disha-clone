import React from 'react'
import './App.css'

// images
import officeImage from './images/office.jpg'

// components
import NavigationBar from './components/navigationBar'
import Button from './components/Button'
import Article from './components/Article'
import StepsList from './components/StepsList'

function App() {
  return (
    <div className="App">
      <NavigationBar />
      <div className="hero">
        <div className="content">
          <h2 className="hero-header">Up your work</h2>
          <p>
            Favour empowers makers and creators with tools and templates to
            showcase their work, save more time and grow their business without
            writing code.
          </p>
          <Button
            background="#FA7F46"
            buttonText="Sign up to Favour Pages"
            width={320}
            height={40}
            marginTop={20}
            borderRadius={7}
          />
        </div>
      </div>
      <div className="header-image"></div>
      <div className="wrapper">
        <div className="parent row-reversed">
          <div className="article-container">
            <Article />
            <div className="article-button">
              <Button
                background="#FA7F46"
                buttonText="Create a Page"
                width={200}
                height={40}
                borderRadius={7}
              />
              <Button
                background="#000"
                buttonText="Learn more"
                width={200}
                height={40}
                borderRadius={7}
              />
            </div>
          </div>
          <div className="body">
            <img alt="middle" src={officeImage} width="100%" />
          </div>
        </div>

        <div className="parent">
          <div className="article-container">
            <Article />
            <div className="article-button">
              <Button
                background="#FA7F46"
                buttonText="Learn more"
                width={200}
                height={40}
                borderRadius={7}
              />
            </div>
          </div>
          <div className="body">
            <img alt="middle" src={officeImage} width="100%" />
          </div>
        </div>
      </div>

      <div
        style={{
          marginTop: '30px',
          background: '#f7f7f7',
          margintop: 20,
          padding: '30px 0',
        }}
      >
        <div className="wrapper">
          <div className="last-article">
            <h2 className="body-sub_text">
              Made for creators.
              <br />
              Designed for you.
            </h2>
            <h3>
              Create, publish and edit a beautiful
              <br />
              responsive website instantly. Zero code/
              <br />
              design thinking required.
            </h3>
          </div>
          <div className="items wrapper">
            <StepsList
              title="Hello there"
              subText="This is what we do to help out."
            />
            <StepsList
              title="Hello there"
              subText="This is what we do to help out."
            />
            <StepsList
              title="Hello there"
              subText="This is what we do to help out."
            />
            <StepsList
              title="Hello there"
              subText="This is what we do to help out."
            />
          </div>
        </div>
      </div>

      <div className="bottom-container">
        <div className="testimonial">
          <h2 className="hero-header">Small team with big ambitions</h2>
          <p>
            Favour empowers makers and creators
            <br /> with tools and templates to showcase
            <br />
            their work, save more time and grow
            <br /> their business without writing code.
          </p>
          <Button
            background="#FA7F46"
            buttonText="More about us"
            width={200}
            marginTop={20}
            height={40}
            borderRadius={7}
          />
        </div>

        <div className="wrapper">
          <div className="card">
            <div className="card-text">
              <h2 className="hero-header">Supported by a community</h2>
              <div
                style={{
                  color: '#fff',
                  fontSize: '20px',
                  marginTop: '20px',
                  marginBottom: '20px',
                }}
              >
                Favour empowers makers and creator with tools and templates to
                showcase their work, save more time and grow their business
                without writing code.
              </div>
            </div>
            <Button
              background="#fff"
              buttonTextColor="#000"
              buttonText="More about us"
              width={200}
              height={40}
              borderRadius={7}
            />
          </div>
        </div>

        <div className="wrapper">
          <div className="footer">
            <div className="footer-logo">
              <h2 style={{ color: '#fff' }}>LOGO</h2>
              <p className="footer-date" style={{ fontSize: '12px' }}>
                2020 Favour Technologies Limited.
                <br /> All rights reserved.
              </p>
            </div>
            <div className="footer-menu">
              <div className="footer-item">
                <p className="footer-menu-title">Hello</p>
                <p>my name is</p>
                <p>Blah blah</p>
              </div>
              <div className="footer-item">
                <p className="footer-menu-title">Hello</p>
                <p>my name is</p>
                <p>blah blah</p>
              </div>
              <div className="footer-item">
                <p className="footer-menu-title">Hello</p>
                <p>my name is</p>
                <p>blah blah</p>
              </div>
              <div className="footer-item">
                <p className="footer-menu-title">Hello</p>
                <p>my name is </p>
                <p>blah blah</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default App
